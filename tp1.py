import itertools
### Logique propositionelle et model checking

## Generation automatique des tables de vérité

# 1 - Fonction de decomposition

def decomp(n: int, nb_bits: int):
    #n : nombre à mettre
    #nb_bit : nombre de bits à mettre
    liste = []
    for i in range(nb_bits):
        liste.append(n%2==1)
        n = n//2
    
    return liste

#print(decomp(3,4))

#2 - Fonction interpretation : crée des dictionnaires

def interpretation(voc, vals):
    return dict(zip(voc,vals))
    
#print(interpretation(["A", "B", "C"],[True, True, False]))

#3 - Generateur d'interpretations

#le nb de lignes est exp par rapport à la taille du voc, on ne peut pas stocker toutes les lignes ni tous les dico
#il faut pas itérer sur les lignes
#yield(i) -> la fct renvoie i, mais l'environement d'execution est gardé en mémoire
#si je veux mettre tous les dics dans une liste : [interpretation(voc, dacomp(len(voc),i)) for i in range(2**len(voc))]
def genInterpretations(voc):
    interpVerity = (dict(zip(voc, x)) for x in itertools.product((False,True), repeat=len(voc)))
    return interpVerity

#print(genInterpretations(["A", "B", "C"]))

#4 - fct valuation
def valuate(formula, interpretation):
    #on utilise eval built-in python : 
        #formula : string ; interpretation : dict = {'a' : False, 'b' : False, 'c': False}
    return eval(formula, interpretation)

#5 - fct permettant de créer la table de vérité

def getTableVerity(voc, vals, formula):

    

